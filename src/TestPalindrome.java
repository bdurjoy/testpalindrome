import java.util.Scanner;


public class TestPalindrome {
	
	public boolean testPalindrome(String strPal) {

		if(strPal.length() == 0 || strPal.length() == 1) {
			return true;
		}
		
		if(strPal.charAt(0) == strPal.charAt(strPal.length()-1)) {
			return testPalindrome(strPal.substring(1, strPal.length()-1));
			
		}
		return false;
	}
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		TestPalindrome palindrome = new TestPalindrome();
		char doAnother;
		do {
			System.out.println("Enter a string: ");
			String strPal = input.nextLine();
			strPal = strPal.replaceAll(" ", "").toLowerCase();
			int start = strPal.charAt(0);
			int end = strPal.length() - 1;
		
			if(palindrome.testPalindrome(strPal) == true) {
				System.out.println("This String is a palindrome");
			}else {
				System.out.println("This String is not a palindrome");
			}
			
			System.out.println("Do another? Y/n");
			doAnother = input.next().charAt(0);
		}while(doAnother == 'y' || doAnother == 'Y');
	}

}
